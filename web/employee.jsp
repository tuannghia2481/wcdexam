<%-- 
    Document   : employee
    Created on : Jan 8, 2020, 8:35:42 AM
    Author     : MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="EmployeeServlet" method="POST">
            Full Name: <input type="text" required="" name="fullname"/><br/>
            Birthday: <input type="text" required="" name="birthday"><br/>
            Address: <input type="text" required="" name="address"><br/>
            Position: <input type="text" required="" name="position"><br/>
            Department: <input type="text" required="" name="department"><br/>
            <input type="submit" value="Submit"/>
            <input type="reset" value="Reset"/>
        </form>
    </body>
</html>
