/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Employee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author MSI
 */
@Local
public interface EmployeeFacadeLocal {

    void create(Employee employee);

    void edit(Employee employee);

    void remove(Employee employee);

    Employee find(Object id);

    List<Employee> findAll();

    List<Employee> findRange(int[] range);

    int count();
    
       
    List<Employee> listEmployee();
    
    boolean addEmployee(String fullname, String birthday , String address , String position , String department);
    
}
